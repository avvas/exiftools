#!/usr/bin/env bash
set -eu -o pipefail
[ $# -ne 1 -o ! -d "${1:-}" ] && exit 1

echo; dir=${1:-/dev/null}
fn_fmt='%y%m%d-%H%M%S%%-c-%Z.%%e'
# c - filename duplicate count (starts at 1)

args=(-preserve -overwrite_original_in_place)
# args+=(-recurse)

args+=(-if '$MIMEType =~ /^(image|video)\//')
# MIME type is parsed from filename extension

echo "- setting correct extensions"

exiftool "${args[@]}" -ext \* \
	'-Filename<%f.$FileTypeExtension' \
	-- "$dir"; echo

echo "- setting date to filenames"

exiftool "${args[@]}" -d "$fn_fmt" \
	'-Filename<FileModifyDate' \
	'-Filename<CreateDate' \
	'-Filename<DateTimeOriginal' \
	-- "$dir"; echo

echo "- suffixing unattributed files"

exiftool "${args[@]}" \
	-if 'not defined $CreateDate and \
		 not defined $DateTimeOriginal' \
	'-Filename=%f.cc.%e' \
	-- "$dir" || :; echo

echo "- removing files trailer"

exiftool "${args[@]}" '-trailer:all=' \
	-- "$dir" || :; echo
